<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['app_edit'] = 'Editar';
$lang['app_create'] = 'Adicionar';
$lang['app_delete'] = 'Excluir';
$lang['app_view'] = 'Visualizar';
$lang['app_actions'] = 'Ações';
$lang['app_cancel'] = 'Cancelar';
$lang['app_back'] = 'Voltar';
$lang['app_reload'] = 'Atualizar';
$lang['app_details'] = 'Detalhes';
$lang['app_attention'] = 'Atenção';
$lang['app_yes'] = 'Sim';
$lang['app_no'] = 'Não';
$lang['app_active'] = 'Ativo';
$lang['app_activate'] = 'Ativar';
$lang['app_inactive'] = 'Inativo';
$lang['app_disable'] = 'Desativar';
$lang['app_check_all'] = 'Marcar Todos';
$lang['app_backup'] = 'Backup';
$lang['app_config'] = 'Configurar';
$lang['app_configs'] = 'Configurações';


// Datatables
$lang['app_display'] = 'Mostrar';
$lang['app_records'] = 'Registros';
$lang['app_per_page'] = 'Mostrar _MENU_ registros por página';
$lang['app_zero_records'] = 'Nenhum registro encontrado.';
$lang['app_empty'] = 'Nenhum registro foi cadastrado.';
$lang['app_filtered'] = '(filtrado de _MAX_ registros)';
$lang['app_showing'] = 'Mostrando página _PAGE_ de _PAGES_';
$lang['app_search'] = 'Pesquisar';
$lang['app_input_search'] = 'Digite o termo a pesquisar';
$lang['app_next'] = 'Próxima';
$lang['app_previous'] = 'Anterior';
$lang['app_first'] = 'Primeira';
$lang['app_last'] = 'Última';
$lang['app_loading'] = 'Carregando...';
$lang['app_processing'] = 'Processando...';


// Messages 
$lang['app_edit_message'] = 'Registro alterado com sucesso.';
$lang['app_add_message'] = 'Registro cadastrado com sucesso.';
$lang['app_delete_message'] = 'Registro excluído com sucesso.';
$lang['app_delete_message_many'] = 'Registros excluídos com sucesso.';
$lang['app_not_found'] = 'Registro não encontrado.';
$lang['app_permission_add'] = 'Você não tem permissão para adicionar';
$lang['app_permission_edit'] = 'Você não tem permissão para editar';
$lang['app_permission_delete'] = 'Você não tem permissão para excluir';
$lang['app_permission_view'] = 'Você não tem permissão para visualizar';
$lang['app_error'] = 'Ocorreu um erro ao executar a ação, por favor tente novamente.';
$lang['app_sure_delete'] = 'Você tem certeza que deseja excluir este registro?';
$lang['app_sure_edit'] = 'Você tem certeza que deseja editar este registro?';
$lang['app_list_updated'] = 'Listagem atualizada.';
$lang['app_data_not_supported'] = 'O sistema não suporta o formato dos dados enviados.';
$lang['app_empty_data'] = 'Lista de dados enviados está vazia.';


// Service Labels
$lang['service'] = 'serviço';
$lang['services'] = 'serviços';
$lang['service_name'] = 'nome';
$lang['service_description'] = 'descrição';
$lang['service_price'] = 'preço';


// Product Labels
$lang['product'] = 'produto';
$lang['products'] = 'produtos';
$lang['product_name'] = 'descrição';
$lang['product_unity'] = 'unidade';
$lang['product_buy_price'] = 'preço de compra';
$lang['product_sell_price'] = 'preço de venda';
$lang['product_stock'] = 'estoque';
$lang['product_min_stock'] = 'estoque mínimo';
$lang['product_mov_type'] = 'tipo de movimento';
$lang['product_out'] = 'saída';
$lang['product_in'] = 'entrada';

// Client Labels
$lang['client'] = 'cliente';
$lang['clients'] = 'clientes';
$lang['client_name'] = 'nome';
$lang['client_sex'] = 'sexo';
$lang['client_type'] = 'tipo de pessoa';
$lang['client_doc'] = 'documento';
$lang['client_phone'] = 'telefone';
$lang['client_cel'] = 'celular';
$lang['client_mail'] = 'e-mail';
$lang['client_created'] = 'data de cadastro';
$lang['client_street'] = 'rua';
$lang['client_number'] = 'numero';
$lang['client_district'] = 'bairro';
$lang['client_city'] = 'cidade';
$lang['client_state'] = 'estado';
$lang['client_zip'] = 'CEP';
$lang['client_obs'] = 'obs';

// Permissions labels
$lang['perm'] = 'permissão';
$lang['perms'] = 'permissões';
$lang['perm_name'] = 'nome da permissão';
$lang['perm_status'] = 'situação';
$lang['perm_created'] = 'criado em';

// Service Labels
$lang['service'] = 'serviço';
$lang['services'] = 'serviços';
$lang[''] = '';

// Product labels
$lang['product'] = 'produto';
$lang['products'] = 'produtos';
$lang[''] = '';

// OS Labels
$lang['os'] = 'ordem de serviço';
$lang['oss'] = 'ordens de serviço';
$lang['os_initial_date'] = 'Data Inicial';
$lang['os_end_date'] = 'Data Final';
$lang['os_defect'] = 'Defeito';

// Report Labels 
$lang['report'] = 'relatório';
$lang['reports'] = 'relatórios';
$lang[''] = '';

// Financial Labels
$lang['payment'] = 'lançamento';
$lang['payments'] = 'lançamentos';
$lang['financial'] = 'financeiro';
$lang[''] = '';
$lang[''] = '';

// File Labels
$lang['file'] = 'arquivo';
$lang['files'] = 'arquivos';
$lang['file_name'] = 'nome';
$lang['file_description'] = 'descrição';
$lang['file_date'] = 'data';
$lang['file_type'] = 'extensão';
$lang['file_size'] = 'tamanho';
$lang['file_download'] = 'download';

// Sale Labels 
$lang['sale'] = 'venda';
$lang['sales'] = 'vendas';
$lang[''] = '';
$lang[''] = '';

// User Labels
$lang['user'] = 'usuário';
$lang['users'] = 'usuários';
$lang['user_name'] = 'Nome';
$lang['user_rg'] = 'RG';
$lang['user_cpf'] = 'CPF';
$lang['user_street'] = 'Rua';
$lang['user_number'] = 'Número';
$lang['user_district'] = 'Bairro';
$lang['user_city'] = 'Cidade';
$lang['user_state'] = 'Estado';
$lang['user_email'] = 'E-mail';
$lang['user_password'] = 'Senha';
$lang['user_phone'] = 'Telefone';
$lang['user_cel'] = 'Celular';
$lang['user_status'] = 'Status';
$lang['user_created'] = 'Data de Cadastro';
$lang['user_group'] = 'Permissão';
$lang['user_change_password'] = 'Preencha caso queira alterar a senha';


// Company Labels
$lang['company'] = 'dados da empresa';
$lang[''] = '';
$lang[''] = '';
