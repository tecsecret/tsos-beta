
## MAPOS - _Versão 4.0_ | Versão em desenvolvimento

MAPOS é um sistema gratuito para de controle de ordens de serviço. 

![MapOS](https://raw.githubusercontent.com/RamonSilva20/mapos/mapos4/assets/images/mapos.png)

Para mais informações visite __[sistemamapos.esy.es](https://www.sistemamapos.esy.es)__ 
ou acesse a __[demo](https://www.sistemamapos.esy.es/mapos)__.  

### Instalação

1. Faça o download dos arquivos.
2. Extraia o pacote e copie para seu webserver.
3. Configure sua URL no arquivo `config.php` alterando a base_url. 
4. Crie o banco de dados e execute o arquivo `banco.sql` para criar as tabelas.
5. Configure os dados de acesso ao banco de dados no arquivo `database.php`.
6. Acesse sua URL e coloque os dados de acesso: `admin@admin.com` e `123456`.

### Roadmap de lançamento da versão 4.0
 - ~~Atualizar framework~~ 
 - Refatorar código e atualizar tema para Bootstrap 4
   - ~~Refatorar painel~~ 
   - ~~Refatorar tela de login~~ 
   - ~~Refatorar cadastro de serviços~~
   - ~~Refatorar cadastro de clientes~~
   - ~~Refatorar cadastro de produtos~~
   - Refatorar cadastro de OS
   - Refatorar cadastro de vendas
   - ~~Refatorar cadastro de arquivos~~
   - Refatorar cadastro de receitas e despesas
   - Refatorar financeiro
   - ~~Refatorar cadastro de usuários~~
   - ~~Refatorar cadastro de permissões~~
   - Refatorar relatórios 
   - Refatorar área do cliente
   - Adicionar validações em javascript
   - Adicionar máscaras nos campos
 - Adicionar notificações por e-mail 
 - Adicionar Rich Text Editor nos campos do tipo TEXTAREA
 - Adicionar desconto e parcelamento na OS e Venda
 - Adicionar quantidade em serviços no cadastro de OS
 - Adicionar cadastro de equipamentos e opção de vincular à OS
 - Adicionar layout de impressão para impressora não fiscal
 - Adicionar layout de impressão em meia folha
 - Adicionar cadastro de status da OS
 - Gerar guia de recebimento
 - Adicionar tarefas à OS

### Frameworks/Bibliotecas
* [bcit-ci/CodeIgniter](https://github.com/bcit-ci/CodeIgniter)
* [twbs/bootstrap](https://github.com/twbs/bootstrap) 
* [jquery/jquery](https://github.com/jquery/jquery) 
* [jquery/jquery-ui](https://github.com/jquery/jquery-ui) 

### Requerimento
* PHP >= 5.4.0
* MySQL

### Contribuidores
* [Ramon Silva](https://github.com/RamonSilva20) - Criador
* [Gianluca Bine](https://github.com/Pr3d4dor)
* [Henrique Miranda](https://github.com/Henrique-Miranda)
* [Mário Lucas](https://github.com/mariolucasdev)
* [Helan Allysson](https://github.com/HelanAllysson)
* [KansasMyers](https://github.com/KansasMyers)
* [drelldeveloper](https://github.com/drelldeveloper) 
* [Samuel Fontebasso](https://github.com/fontebasso)
* [marllonferreira](https://github.com/marllonferreira)
